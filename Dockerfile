FROM denoland/deno:alpine-1.26.0

# 建立一個資料夾
WORKDIR /app 

# 複製檔案到資料夾
COPY . /app 

# 設定 port
EXPOSE 8000

# 執行指令
RUN deno cache main.ts

# 把 server 跑起來
CMD ["run", "--allow-all", "main.ts"]
